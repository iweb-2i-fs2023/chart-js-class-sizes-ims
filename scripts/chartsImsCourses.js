const ID_CHART_COURSES = 'progress-ims-courses';

/**
 * Erstellt Balkendiagramm für die Entwicklung der IMS Klassen
 * Grösse.
 * @param courseData: Array, dessen Elemente ein Objekt mit
 * den Daten für jedes Schuljahr. Im Objekt findet man die
 * Bezeichnung des Schuljahrs (Property schoolYear) und ein
 * Array mit den Klassengrössen der einzelnen Klassen 1i, 2i
 * und 3i (Property classSizes).
 */
function createImsCourseChart(courseData) {
  /* 1. Parmeter: Canvas Element in HTML für  
     2. Parameter: Objekt mit ...
        ... Art des Diagramms (type)
        ... Konfiguration des Diagramms (options)
        ... Dargestellte Daten (data)
  */
  new Chart(document.getElementById(ID_CHART_COURSES), {
    type: 'bar',
    /* Konfiguration */
    options: {
      animation: true,
      scales: {
        x: {
          ticks: {
            color: 'rgba(0, 0, 0, 0.7)',
          },
          border: {
            display: false,
          },
          grid: {
            display: false,
          },
        },
        y: {
          ticks: {
            color: 'rgba(0, 0, 0, 0.7)',
          },
          border: {
            color: 'rgba(0, 0, 0, 0.2)',
          },
          grid: {
            color: 'rgba(0, 0, 0, 0.2)',
          },
        },
      },
    },
    /* Daten */
    data: {
      /* Bezeichnung der Schuljahre (z. B. SJ 20/21) wird als
         Label verwendet. Die map-Funktion extrahiert aus den
         einzelnen Array Objekten nur die Bezeichnung des
         Schuljahres und erzeugt daraus einen neuen Array der
         nur diese Bezeichnungen enthält.
       */
      labels: courseData.map((yearData) => yearData.schoolYear),
      datasets: [
        {
          label: '1i',
          /* Die Klassengrössen für die Klasse 1i über alle
             Schuljahre werden dargestellt. Die map-Funktion
             extrahiert aus den einzelnen Array Objekten der
             Schuljahre das erste Element aus dem Klassengrössen
             Array, welches der Klasse 1i entspricht und erzeugt
             einen neuen Array der nur die Klassengrösse vom 1i
             über alle Schuljahre hinweg enthält.
           */
          data: courseData.map((yearData) => yearData.classSizes[0]),
        },
        {
          label: '2i',
          /* Die Klassengrössen für die Klasse 2i über alle
             Schuljahre werden dargestellt. 
           */
          data: courseData.map((yearData) => yearData.classSizes[1]),
        },
        {
          label: '3i',
          /* Die Klassengrössen für die Klasse 3i über alle
             Schuljahre werden dargestellt. 
           */
          data: courseData.map((yearData) => yearData.classSizes[2]),
        },
      ],
    },
  });
}

Chart.defaults.font.family = "'Helvetica Narrow', 'Arial Narrow', sans-serif";
Chart.defaults.font.size = 16;

export { createImsCourseChart };
