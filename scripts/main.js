import { getImsCourses } from './imsCoursesService.js';
import { createImsCourseChart } from './chartsImsCourses.js';

getImsCourses().then((imsCourses) => {
  createImsCourseChart(imsCourses);
});
